# AWSToolkit README #

This is a simple library/interface to easily access and manipulate AWS Resources like S3 and DyanmoDB.

## SETUP ##

* Clone the repo
* Install Jdk 8
* Import into Intellij using Gradle and Auto-import (Verified works on Intellij 13-15)
* Check project settings are set to Jdk8
* Check that project has refreshed the Gradle dependencies (Gradle Tools Window -> can be found with Ctrl + Shift + a) Little blue refresh icon near top left.
* Setup AWS Credential File (~/.aws/credential) The file (all examples have no extension on the file) has the following values: 
    `[default]`
    aws_access_key_id = someKey
    aws_secret_access_key = someSecret

### Resources ###

* [Spring Boot Getting Started Guide](https://spring.io/guides/gs/spring-boot/)
* [Groovy Template Engine + Spring Boot](https://spring.io/blog/2014/05/28/using-the-innovative-groovy-template-engine-in-spring-boot)
* [AWS Credential Setup](http://docs.aws.amazon.com/AWSSdkDocsJava/latest/DeveloperGuide/credentials.html#id1)