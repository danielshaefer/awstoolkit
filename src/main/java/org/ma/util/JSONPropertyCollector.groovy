package org.ma.util

import com.amazonaws.util.json.JSONObject

class JSONPropertyCollector {
    
    public List<String> getProperty(List<JSONObject> jsonObjectList, String propertyName) {
        jsonObjectList.collect{it.getString(propertyName)}
    }

    public String getPropertyAsString(List<JSONObject> jsonObjectList, String propertyName) {
        jsonObjectList.collect{it.getString(propertyName)}.join(",")
    }
    
}
