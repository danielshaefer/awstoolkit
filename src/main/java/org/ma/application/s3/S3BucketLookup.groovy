package org.ma.application.s3

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.S3ObjectInputStream
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.amazonaws.util.json.JSONException
import com.amazonaws.util.json.JSONObject
import org.ma.util.InputStreamToString

class S3BucketLookup {
    
    private String jsonFileExtension = ".json"
    
    public List<JSONObject> getJsonFromBucket(String bucketName, String prefix) throws IOException, JSONException {
        def s3Json = getJsonStringsFromBucket(bucketName, prefix)
        s3Json.collect{new JSONObject(it.jsonString)}
    }

    public List<S3Json> getJsonStringsFromBucket(String bucketName, String bucketPrefix, int numOfItems) throws IOException, JSONException {
        AmazonS3 s3Client = new AmazonS3Client();
        ObjectListing listing = s3Client.listObjects(bucketName, bucketPrefix);

        def jsonStrings = []
        for(S3ObjectSummary objectSummary: listing.getObjectSummaries()) {
            if (jsonStrings.size() == numOfItems) break;
            
            String key = objectSummary.getKey();
            if (!key.endsWith(jsonFileExtension))
                continue;

            S3Object obj = s3Client.getObject(bucketName, key);
            S3ObjectInputStream objectData = obj.getObjectContent();
            String jsonString = new InputStreamToString().from(objectData);
            jsonStrings += new S3Json(name: removePathPrefixAndFileExtension(key, bucketPrefix, jsonFileExtension), jsonString: jsonString)
        }
        jsonStrings.sort{S3Json a, S3Json b -> a.name <=> b.name}
        return jsonStrings;
    }

    String removePathPrefixAndFileExtension(String key, String prefix, String suffix) {
        key[prefix.size()+1..-(suffix.size()+1)]
    }
}