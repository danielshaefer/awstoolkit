package org.ma.application.s3

import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.util.json.JSONArray

class DynamoDBUploader {
    
    public void uploadTrimmedDownSpellsData(List<S3Json> s3Json) {
        String tableName = "Spells"
        DynamoDB dynamo = new DynamoDB(new AmazonDynamoDBClient().withRegion(Regions.US_WEST_2));

        Table table = dynamo.getTable(tableName);

        s3Json.eachWithIndex { it, i ->
            def item = Item.fromJSON(it.jsonString)
            item.withPrimaryKey("name", it.name)
            item.withInt("id", i)
            item.removeAttribute("components")
            item.removeAttribute("levels")
            item.removeAttribute("subschool")
            item.removeAttribute("descriptor")
            item.removeAttribute("url")
            
            def effects = item.getJSON("effects")
            if (effects) {
                def effectsArray = new JSONArray(effects)
                def effectsNameAndText = ""
                def delim = ""
                for (def j = 0; j < effectsArray.length(); j++) {
                    def obj = effectsArray.getJSONObject(j)
                    effectsNameAndText += """$delim${obj.getString("name")}: ${obj.getString("text")}"""
                    delim = ", "
                }
                item.withString("effects_text", effectsNameAndText)
            }
            item.removeAttribute("effects")
            
            table.putItem(item);
            println("Added item $it.name to dynamo")
        }
    }
    
    public void uploadTableOfJsonDocuments(String tableName, List<S3Json> s3Json) {
        DynamoDB dynamo = new DynamoDB(new AmazonDynamoDBClient().withRegion(Regions.US_WEST_2));

        Table table = dynamo.getTable(tableName);

        s3Json.eachWithIndex { it, i ->
            def item = new Item()
                    .withPrimaryKey("name", it.name)
                    .withJSON("document", it.jsonString)

            table.putItem(item);
            println("Added item $it.name to dynamo")
        }
    }
    
}