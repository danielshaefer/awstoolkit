package org.ma.application.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import org.ma.util.InputStreamToString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class S3Lookup {
    
    public List<JSONObject> getJsonFromBucket(String bucketName, String prefix) throws IOException, JSONException {
        AmazonS3 s3Client = new AmazonS3Client();
        ObjectListing listing = s3Client.listObjects(bucketName, prefix);
        
        List<JSONObject> objects = new ArrayList<JSONObject>();
        for(S3ObjectSummary objectSummary: listing.getObjectSummaries()) {
            String key = objectSummary.getKey();
            if (!key.endsWith(".json"))
                continue;
            if (objects.size() == 10)
                break;
            S3Object obj = s3Client.getObject(bucketName, key);
            S3ObjectInputStream objectData = obj.getObjectContent();
            String jsonString = new InputStreamToString().from(objectData);
            objects.add(new JSONObject(jsonString));
        }
        return objects;
    }
    
    public List<String> getJsonStringsFromBucket(String bucketName, String prefix) throws IOException, JSONException {
        AmazonS3 s3Client = new AmazonS3Client();
        ObjectListing listing = s3Client.listObjects(bucketName, prefix);

        List<String> objects = new ArrayList<String>();
        for(S3ObjectSummary objectSummary: listing.getObjectSummaries()) {
            String key = objectSummary.getKey();
            if (!key.endsWith(".json"))
                continue;
            if (objects.size() == 10)
                break;
            S3Object obj = s3Client.getObject(bucketName, key);
            S3ObjectInputStream objectData = obj.getObjectContent();
            String jsonString = new InputStreamToString().from(objectData);
            objects.add(jsonString);
        }
        return objects;
    }

}