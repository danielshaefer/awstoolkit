package org.ma.controller;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import org.ma.application.s3.S3BucketLookup;
import org.ma.util.JSONPropertyCollector;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class TestController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot Root Controller!";
    }

    @RequestMapping(value={"/test", "/test/"}) //this solves trailing slash problems.
    String test() {
        return "contact aws and get some s3 info";
    }

    @RequestMapping("/s3")
    String s3ToDynamo() throws IOException, JSONException {
        //http://docs.aws.amazon.com/AWSSdkDocsJava/latest/DeveloperGuide/credentials.html#id1
        String bucketName = "monsteradvancer";
        String prefix = "pfsrd/spells/core_rulebook";
        List<JSONObject> objects = new S3BucketLookup().getJsonFromBucket(bucketName, prefix);
        return new JSONPropertyCollector().getPropertyAsString(objects, "name");
        //AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient();
        //return "contact aws and get some s3 info";
    }
}