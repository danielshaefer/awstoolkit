package org.ma.controller

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.Table
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec
import org.ma.application.s3.DynamoDBUploader
import org.ma.application.s3.S3BucketLookup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.Banner
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

@RestController
@RequestMapping("/")
class S3Controller {

    private RequestMappingHandlerMapping requestMapping

    @Autowired
    public S3Controller(RequestMappingHandlerMapping requestMapping) {
        this.requestMapping = requestMapping
    }
    
    @RequestMapping("/")
    String index() {
        def endpoints = requestMapping.getHandlerMethods().collect{it.key.patternsCondition.patterns}.join(",<br>")
        "Mapped Endpoints: $endpoints"
        
    }
    
    @RequestMapping("/dynamoUpload")
    String s3ToDynamo() {
        String bucketName = "monsteradvancer";
        String prefix = "pfsrd/spells/core_rulebook";
        int numOfItems = 700
        def s3Json = new S3BucketLookup().getJsonStringsFromBucket(bucketName, prefix, numOfItems);

        new DynamoDBUploader().uploadTableOfJsonDocuments("Spells_Docs", s3Json)
        return "Completed ${s3Json.size()} items"
    }

    @RequestMapping("/dynamoRead")
    String fromDynamo() {
        String tableName = "Spells";
        
        DynamoDB dynamo = new DynamoDB(new AmazonDynamoDBClient());
        Table table = dynamo.getTable(tableName);

        Item documentItem =
                table.getItem(new GetItemSpec()
                        .withPrimaryKey("name", "Acid Arrow")
                        .withAttributesToGet("document"));

        System.out.println(documentItem.getJSONPretty("document"));
    }

    @RequestMapping(["/home", "/home/"])
    def home() {
        new ModelAndView(
                "views/home",
                [bootVersion: Banner.package.implementationVersion,
                 groovyVersion: GroovySystem.version])
    }
    
}
